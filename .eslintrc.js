module.exports = {
  root: true,
  extends: "@react-native-community",

  env: {
    node: 1
  },
  rules: {
    quotes: [2, "double", { avoidEscape: true }]
  }
};
