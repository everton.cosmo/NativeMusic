/* eslint-disable comma-dangle */
import React, { Component } from "react";
import { Provider } from "react-redux";

// import AppContainer from "./AppContainer";

import { configureStore } from "./src/store";
import Root from "./Root";
const { store } = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Root />
      </Provider>
    );
  }
}
