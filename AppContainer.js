/* eslint-disable comma-dangle */
import { createStackNavigator, createAppContainer } from "react-navigation";
import { HomeScreen } from "./src/screens";
import Player from "./src/components/Player";

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: { title: "Native Music" }
  },
  Player: {
    screen: Player
  }
});

const AppNavigation = createAppContainer(AppNavigator);

export default AppNavigation;
