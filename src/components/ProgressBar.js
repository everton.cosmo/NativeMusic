/* eslint-disable comma-dangle */
/* eslint-disable react-native/no-inline-styles */
import React from "react";
import { ProgressComponent } from "react-native-track-player";
import { View, StyleSheet } from "react-native";
import { ProgressBar as ProgressBarPaper, Caption } from "react-native-paper";

export default class ProgressBar extends ProgressComponent {
  // HH:mm:ss
  formatTime = seconds => {
    const ss = Math.floor(seconds) % 60;
    const mm = Math.floor(seconds / 60) % 60;
    const hh = Math.floor(seconds / 3600);

    if (hh > 0) {
      return (
        hh + ":" + this.formatTwoDigits(mm) + ":" + this.formatTwoDigits(ss)
      );
    } else {
      return mm + ":" + this.formatTwoDigits(ss);
    }
  };
  formatTwoDigits = n => {
    return n < 10 ? "0" + n : n;
  };

  render() {
    const duration = this.formatTime(Math.floor(this.state.duration));
    const position = this.formatTime(Math.floor(this.state.position));
    const info = position + " / " + duration;

    return (
      <View style={styles.progress}>
        <Caption>{info}</Caption>
        <ProgressBarPaper
          style={{ width: 200 }}
          progress={this.getProgress()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  progress: {
    flexDirection: "column",
    flex: 1,
    width: "100%",
    alignItems: "center",
    alignContent: "center"
  }
});
