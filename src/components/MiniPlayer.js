import React, { Component } from "react";

import { View } from "react-native";
import TrackPlayer from "react-native-track-player";
import { TouchableOpacity } from "react-native-gesture-handler";
import FastImage from "react-native-fast-image";
import { Title, Caption, IconButton, Surface } from "react-native-paper";

export default class MiniPlayer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { currentTrack, navigate, state } = this.props;

    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.6}
          style={{ width: "100%" }}
          onPress={() => {
            navigate("Player");
          }}
        >
          <Surface>
            {/* Image cover of current track  */}
            <View>
              <FastImage
                source={{ uri: currentTrack.artwork }}
                style={{ width: 50, height: 50, borderRadius: 5 }}
              />
            </View>

            {/* CurrentTrack Playing */}
            <View>
              <Title>{currentTrack.title}</Title>
              <Caption>
                {currentTrack.artist ? currentTrack.artist : currentTrack.album}
              </Caption>
            </View>

            <View style={{ alignContent: "center", alignItems: "center" }}>
              <IconButton
                icon={
                  state == TrackPlayer.STATE_PLAYING ? "pause" : "play-arrow"
                }
                size={30}
                onPress={this.props.onPress()}
                animated={true}
              />
            </View>
          </Surface>
        </TouchableOpacity>
      </View>
    );
  }
}
