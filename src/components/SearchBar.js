import React, { Component } from "react";

import { View } from "react-native";
import { Searchbar as SearchBarPaper } from "react-native-paper";
// import { Container } from './styles';

export default class SearchBar extends Component {
  render() {
    return <SearchBarPaper placeholder="Search Music" />;
  }
}
