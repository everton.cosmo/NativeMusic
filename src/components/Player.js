/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
import React, { Component } from "react";
import { View, Text, StatusBar, ScrollView, Image } from "react-native";
import { connect } from "react-redux";
import TrackPlayer from "react-native-track-player";
import FastImage from "react-native-fast-image";
import { updateCurrentTrack, updateStatus } from "../store/actions/player";
import { Controls } from ".";
import { ProgressBar } from ".";

class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PlayImage: require("../img/poster.jpg")
    };
  }
  componentDidMount() {
    TrackPlayer.setupPlayer().then(() => {
      TrackPlayer.updateOptions({
        stopWithApp: false,
        capabilities: [
          TrackPlayer.CAPABILITY_PLAY,
          TrackPlayer.CAPABILITY_PAUSE,
          TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
          TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
          TrackPlayer.CAPABILITY_SEEK_TO
        ]
      });
    });
    this.updateTrack();
    this.updateUI();
    this.errorHandler();
  }
  componentWillUnmount() {
    this.updateTrack.remove();
    this.updateTrackUI.remove();
  }
  updateTrack = async () =>
    TrackPlayer.addEventListener("playback-track-changed", async data => {
      this.props.updateCurrentTrack(data.nextTrack);
    });

  updateTrackUI = TrackPlayer.addEventListener("playback-state", async data => {
    switch (data.state) {
      case TrackPlayer.STATE_PLAYING:
        this.props.updateStatus(TrackPlayer.STATE_PLAYING);
        break;
      case TrackPlayer.STATE_PAUSED:
        this.props.updateStatus(TrackPlayer.STATE_PAUSED);
        break;
      case TrackPlayer.STATE_BUFFERING:
        this.props.updateStatus(TrackPlayer.STATE_BUFFERING);
    }
  });
  errorHandler = TrackPlayer.addEventListener("playback-error", async data => {
    alert(data.message);
  });

  tooglePlayback = () => {
    try {
      TrackPlayer.getCurrentTrack().then(track => {
        if (!_.isNull(track)) {
          if (this.props.state === TrackPlayer.STATE_PLAYING) {
            TrackPlayer.play();
          } else {
            TrackPlayer.pause();
          }
        } else {
          TrackPlayer.reset();
          TrackPlayer.add(track);
          TrackPlayer.play();
        }
      });
    } catch (err) {
      console.log(err);
    }
  };
  skipNext = async () => {
    try {
      TrackPlayer.skipToNext().then(() => TrackPlayer.play());
    } catch (err) {
      alert(err);
      TrackPlayer.stop();
    }
  };
  skipPrevious = async () => {
    try {
      TrackPlayer.skipToPrevious().then(() => TrackPlayer.play());
      this.updateTrack();
    } catch (err) {
      alert(err);
      TrackPlayer.stop();
    }
  };

  render() {
    const { currentTrack } = this.props;
    return (
      <View>
        <View>
          <FastImage
            source={{ uri: currentTrack.artwork }}
            style={{ width: 200 }}
          />
        </View>
        <ProgressBar />
        <View style={{ flex: 1, alignContent: "center", alignItems: "center" }}>
          <Controls
            status={this.props.state}
            onPress={this.skipPrevious()}
            icon={"previous"}
          />
          <Controls
            status={this.props.state}
            onPress={this.tooglePlayback()}
            icon={
              this.props.state === TrackPlayer.STATE_PLAYING ? "pause" : "play"
            }
          />
          <Controls
            status={this.props.state}
            onPress={this.skipNext()}
            icon={"next"}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  // tracks: state.mediaStorage.tracks,
  currentTrack: state.player.currentTrack,
  state: state.player.state
});
export default connect(
  mapStateToProps,
  { updateCurrentTrack, updateStatus }
)(Player);
