import React, { PureComponent } from "react";
import { StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import { List } from "react-native-paper";
import { playTrack } from "../store/actions/player";
import FastImage from "react-native-fast-image";
import Icon from "react-native-vector-icons";
import posterImage from "../img/poster.jpg";
import TrackPlayer from "react-native-track-player";
import { updateCurrentTrack } from "../store/actions/player";
import Player from "./Player";
import _ from "lodash";
class Track extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageSong: "../img/poster.jpg"
    };
  }
  playSong = () => {
    const { track, currentTrack } = this.props;

    if (!_.isNull(currentTrack)) {
      if (track === currentTrack) {
        return false;
      }
      this.props.playTrack(track);
      navigation.navigate("Player");
    }
  };

  renderIconLeft = props => {
    const { track } = this.props;
    // alert(track.artwork);
    return (
      // <List.Item {...props}>

      /* // <FastImage source={posterImage} /> */

      <Image source={posterImage} style={{ width: 100, height: 100 }} />
      // </List.Item>
    );
  };
  renderIconRight = props => {
    const { track, currentTrack } = this.props;
    if (track) {
      if (track === currentTrack) {
        return <List.Icon {...props} key={track.id} icon="pause" />;
      } else {
        return <List.Icon {...props} key={track.id} icon="play-arrow" />;
      }
    }
  };
  render() {
    const { track } = this.props;
    // alert(track);
    return (
      <List.Item
        style={{ padding: 10 }}
        item={track}
        title={track.title}
        description={track.album ? track.album : track.artist}
        left={props => this.renderIconLeft(props)}
        right={props => this.renderIconRight(props)}
        onPress={() => this.playSong()}
      />
    );
  }
}
const mapStateToProps = state => ({
  currentTrack: state.player.currentTrack
});

export default connect(
  mapStateToProps,
  { playTrack, updateCurrentTrack }
)(Track);

const styles = StyleSheet.create({});
