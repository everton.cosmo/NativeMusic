/* eslint-disable comma-dangle */
import React from "react";

import { FAB, IconButton } from "react-native-paper";
import { connect } from "react-redux";
import TrackPlayer from "react-native-track-player";
import { updateStatus } from "../store/actions/player";

const Controls = ({ status, onPress, icon, updateStatus }) => {
  if (
    status === TrackPlayer.STATE_PAUSED ||
    status === TrackPlayer.STATE_PLAYING
  ) {
    <FAB icon={icon} size={35} onPress={onPress} />;
    updateStatus(status);
  } else {
    <IconButton icon={icon} size={35} onPress={onPress} />;
    updateStatus(status);
  }
};

export default connect(
  null,
  { updateStatus }
)(Controls);
