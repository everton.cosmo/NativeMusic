/* eslint-disable comma-dangle */
import React, { Component } from "react";
import { connect } from "react-redux";

import Track from "../../components/Track";
import { getAllSongs, setTracks } from "../../store/actions/storage";
import { addQueue } from "../../store/actions/player";
import {
  FlatList,
  Text,
  View,
  PermissionsAndroid,
  DeviceEventEmitter
} from "react-native";
import { ActivityIndicator, Colors, Divider } from "react-native-paper";

import _ from "lodash";
import { SearchBar } from "../../components/";
class Home extends Component {
  constructor(props) {
    super(props);

    this.requestPermission = async () => {
      try {
        const granted = await PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
          ],
          {
            title: "Permission",
            message: "Storage access is requiered",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          alert("You can use the package");
        } else {
          this.requestPermission();
        }
      } catch (err) {
        console.warn(err);
      }
    };
  }

  componentDidMount() {
    this.requestPermission();
    this.props.getAllSongs();

    this.props.addQueue(this.props.tracks);

    // alert(this.props.tracks);
    // DeviceEventEmitter.addListener("onBatchReceived", p => {
    //   this.props.setTracks([...this.props.tracks, p.batch]);
    // });
    // DeviceEventEmitter.addListener("onLastBatchReceived", params => {
    //   this.setState(alert("last batch sent"));
    // });
  }

  indicatorFetching = () => {
    <ActivityIndicator animating={this.props.tracks} color={Colors.green100} />;
  };
  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }
  dividerRender = () => <Divider inset={true} />;
  renderItem({ item }) {
    return <Track track={item} />;
  }

  render() {
    const { tracks } = this.props;
    return (
      // <View />
      // <View>

      <View style={{ flex: 1, backgroundColor: "#dedede" }}>
        <SearchBar style={{ padding: 10 }} />
        <FlatList
          data={tracks}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.dividerRender}
          renderItem={this.renderItem}
        />
      </View>
      // </View>
    );
  }
}

const mapStateToProps = state => ({
  tracks: state.mediaStorage.tracks,
  fetching: state.mediaStorage.fetching
});
export default connect(
  mapStateToProps,
  { getAllSongs, addQueue }
)(Home);
