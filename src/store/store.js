import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "./reducers";
import thunk from "redux-thunk";
import { composeWithDevTools } from "remote-redux-devtools";

const middleware = [thunk];
const compose = composeWithDevTools({ realtime: true });
// configure store
export default () => {
  let store = createStore(rootReducer, compose(applyMiddleware(...middleware)));
  return { store };
};
