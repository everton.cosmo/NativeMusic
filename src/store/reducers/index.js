/* eslint-disable comma-dangle */
import { combineReducers } from "redux";
import player from "./player";
import mediaStorage from "./mediaStorage";
export const rootReducer = combineReducers({
  mediaStorage: mediaStorage,
  player: player
});
