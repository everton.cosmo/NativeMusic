import { FETCH_FILES, ERROR } from "../actions";

const INITIAL_STORAGE = {
  fetching: false,
  tracks: [],
  artists: [],
  albums: []
};
export default (state = INITIAL_STORAGE, action) => {
  switch (action.type) {
    case FETCH_FILES:
      // alert(action.tracks);
      return { ...state, tracks: action.tracks, fetching: action.fetching };
    case ERROR:
      return { ...state, valueGet: action.value, fetching: action.fetching };
    default:
      return state;
  }
};
