import {
  UPDATE_CURRENT_TRACK,
  STATUS_PLAYER,
  PLAY,
  ADD_TO_QUEUE
} from "../actions";

const INITIAL_STATE = {
  currentTrack: {},
  queue: [],
  state: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PLAY:
      return { ...state, currentTrack: action.track };
    case UPDATE_CURRENT_TRACK:
      return { ...state, currentTrack: action.newTrack };
    case STATUS_PLAYER:
      return { ...state, state: action.state };
    case ADD_TO_QUEUE:
      return { ...state, queue: action.queue };
    default:
      return state;
  }
};
