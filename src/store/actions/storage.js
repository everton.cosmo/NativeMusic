/* eslint-disable no-alert */
import { FETCH_FILES, ERROR } from ".";
import { PermissionsAndroid } from "react-native";
import RNAndroidAudioStore from "react-native-get-music-files";
import _ from "lodash";

export const getAllSongs = () => dispatch => {
  try {
    RNAndroidAudioStore.getAll({}).then(files => {
      _.map(files, track => {
        track.id = track.path;
        track.artwork = track.cover
          ? track.cover
          : require("../../img/poster.jpg");

        track.url = "file://" + track.path;
        track.title = track.fileName;

        delete track.path;
        return track;
      });

      dispatch({
        type: FETCH_FILES,
        tracks: files,
        fetching: true
      });
    });
  } catch (err) {
    console.log(err);
  }
};
export const setTracks = tracks => dispatch => {
  if (!_.isNull(tracks))
    dispatch({ type: FETCH_FILES, tracks: tracks, fetching: true });
  else alert("error fetching tracks");
};
