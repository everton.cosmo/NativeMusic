export const STATUS_PLAYER = "STATUS_PLAYER";
export const PLAY = "PLAY";
export const NEXT = "NEXT";
export const PREVIOUS = "PREVIOUS";
export const FETCH_FILES = "FETCH_FILES";
export const ERROR = "ERROR";
export const UPDATE_CURRENT_TRACK = "UPDATE_CURRENT_TRACK";

export const ADD_TO_QUEUE = "ADD_TO_QUEUE";
