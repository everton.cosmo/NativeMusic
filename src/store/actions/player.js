import _ from "lodash";
import TrackPlayer from "react-native-track-player";
import {
  UPDATE_CURRENT_TRACK,
  STATUS_PLAYER,
  PLAY,
  ADD_TO_QUEUE,
  NEXT
} from ".";

export const updateCurrentTrack = track => dispatch => {
  if (!_.isEmpty(track)) {
    dispatch({ type: UPDATE_CURRENT_TRACK, newTrack: track });
  }
};
export const updateStatus = state => dispatch => {
  dispatch({ type: STATUS_PLAYER, state: state });
};

export const playTrack = track => dispatch => {
  if (!_.isNull(track)) {
    TrackPlayer.getCurrentTrack()
      .then(current => {
        if (_.isNull(current)) {
          TrackPlayer.skip(track.id)
            .then(() => {
              TrackPlayer.play();
              dispatch({ type: PLAY, track: track });
            })
            .catch(() => {
              // alert(err);
              TrackPlayer.add(track).then(() => {
                TrackPlayer.skip(track.id).then(() => {
                  TrackPlayer.play();
                  dispatch({ type: PLAY, track: track });
                });
              });
            });
        } else {
          let songs = [current, track];
          TrackPlayer.add(songs).then(() => {
            TrackPlayer.skip(track.id)
              .then(() => {
                TrackPlayer.play();
                dispatch({ type: PLAY, track: track });
              })
              .catch(() => {
                TrackPlayer.skipToNext();
              });
          });
        }
      })
      .catch(err => {
        alert(err);
        TrackPlayer.add(track);
        TrackPlayer.skip(track.id).then(() => {
          TrackPlayer.play();
          dispatch({ type: PLAY, track: track });
        });
      });
  }
};

export const updateQueue = queue => dispatch => {
  if (_.isEmpty(queue)) {
    TrackPlayer.reset();
    TrackPlayer.add(queue);
  }
};
export const addQueue = songs => dispatch => {
  if (_.isArray(songs)) {
    TrackPlayer.getQueue()
      .then(queue => {
        if (_.isEmpty(queue)) {
          TrackPlayer.add(songs);
          TrackPlayer.play();
          dispatch({ type: ADD_TO_QUEUE, queue: songs });
        } else {
          TrackPlayer.add(songs);
          TrackPlayer.play();
          dispatch({ type: ADD_TO_QUEUE, queue: _.concat(queue, songs) });
        }
      })
      .catch(() => {
        alert("Error adding queue");

        // TrackPlayer.play();
      });
  }
};
