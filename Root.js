/* eslint-disable comma-dangle */
import React, { Component } from "react";
import { Provider as ProviderPaper } from "react-native-paper";
import AppNavigation from "./AppContainer";
export default class Root extends Component {
  render() {
    return (
      <ProviderPaper>
        <AppNavigation />
      </ProviderPaper>
    );
  }
}
